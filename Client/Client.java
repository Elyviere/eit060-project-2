package Client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.KeyStore;
import java.util.ArrayList;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import Libs.ClientMessage;
import Libs.Record;
import Libs.ServerMessage;

public class Client {

	public static void main(String[] args) throws Exception {
		String host = null;
		int port = -1;

		if (args.length < 2) {
			System.out.println("USAGE: java client host port");
			System.exit(-1);
		}
		try { /* get input parameters */
			host = args[0];
			port = Integer.parseInt(args[1]);
		} catch (IllegalArgumentException e) {
			System.out.println("USAGE: java client host port");
			System.exit(-1);
		}

		try { /* set up a key manager for client authentication */
			SSLSocketFactory factory = null;
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(
						System.in));
				Console console = System.console();// Kommer kunna användas om
				//man kör programmet från terminalen.
				// Men kan inte kompilera av någon anledning. :(
				if (console == null) {
				 System.out.println("Couldn't get Console instance");
				 System.exit(0);
				 }
				//String username;
				//char[] password;
				System.out.print("Type your username: ");
				//username = br.readLine();
				String username = console.readLine();
				System.out.print("Type your password: ");
				//password = br.readLine().toCharArray();
				 char[] password = console.readPassword();
				KeyStore ks = KeyStore.getInstance("JKS");
				KeyStore ts = KeyStore.getInstance("JKS");
				KeyManagerFactory kmf = KeyManagerFactory
						.getInstance("SunX509");
				TrustManagerFactory tmf = TrustManagerFactory
						.getInstance("SunX509");
				SSLContext ctx = SSLContext.getInstance("TLS");

				ks.load(new FileInputStream(System.getProperty("user.dir")
						+ "/Client/Stores/" + username + "keystore"), password); // keystore
				// password
				// (storepass)
				ts.load(new FileInputStream(System.getProperty("user.dir")
						+ "/Client/Stores/" + username + "truststore"),
						password); // truststore
				// password
				// (storepass);
				kmf.init(ks, password); // user password (keypass)
				tmf.init(ts); // keystore can be used as truststore here
				ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
				factory = ctx.getSocketFactory();
			} catch (Exception e) {
				System.out.println("Wrong password or Username, please re enter.");
				String[] startArgs={"localhost","9876"};
				Client.main(startArgs);
			}
			SSLSocket socket = (SSLSocket) factory.createSocket(host, port);

			/*
			 * send http request
			 * 
			 * See SSLSocketClient.java for more information about why there is
			 * a forced handshake here when using PrintWriters.
			 */

			try {
				socket.startHandshake();
			} catch (SSLHandshakeException e) {
				System.err.println("User unauthorised");
				System.exit(0);
			}

			SSLSession session = socket.getSession();
			BufferedReader read = new BufferedReader(new InputStreamReader(
					System.in));
			ObjectOutputStream out = new ObjectOutputStream(
					socket.getOutputStream());
			ObjectInputStream in = new ObjectInputStream(
					socket.getInputStream());
			String inputMessage;
			ClientMessage msg;
			ArrayList<Record> records=null;
			System.out.println("Your are now connected to the server\n");
			do {

				System.out.println("Please type one of the commands below, enclosed in \"\" (\"\" should not be included in the command).  \nAnything enclosed in [] brackets should be replaced by what is mentioned in the brackets. \nThe brackets themselves should not be included in the command.");
				System.out.println("Exit - \"0\"");
				System.out.println("Get journal(s) - \"1\"");
				System.out
						.println("Write journal (Nurse/Doctor/GA) - \"2;[Patient name];[Journal ID]\"");
				System.out
						.println("Create new journal (Doctor/GA) - \"3;[Patient name];[Nurse name]\"");
				System.out.println("Delete journal (GA) - \"4;[Patient name];[Journal ID]\"");
				System.out.print("> ");
				inputMessage = read.readLine();

				if(records==null){
				msg = new ClientMessage(inputMessage,null);
				} else {
					msg = new ClientMessage(inputMessage,records);
				}
				while (msg == null) {
					System.out
							.println("'"
									+ inputMessage.substring(0, 1)
									+ "' är inte ett korrekt commando. Skriv ett av commandona 0-4.");
					inputMessage = read.readLine();
					if(records==null){
						msg = new ClientMessage(inputMessage,null);
						} else {
							msg = new ClientMessage(inputMessage,records);
						}
				}
				out.writeObject(msg);
				out.flush();
				if (msg.getCmd() != 0){
					ServerMessage message = (ServerMessage) in.readObject();
					records = message.getRecords();
					if (records == null) {
						System.out.println(message.getMessage());
					} else {
						
						for(Record r: records){
							String filePath = System.getProperty("user.dir") + "/Client/Records/"+r.getPatient()+"-"+r.getId();
							
							File file = new File(filePath);
							if (!file.isFile()) {
								try {
									file.createNewFile();
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
							try {
								BufferedWriter writer = new BufferedWriter(
										new FileWriter(file));
								writer.write(r.getData());
								writer.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						System.out.println(message.getMessage()+"\nDe ligger i: "+System.getProperty("user.dir") + "/Client/Records/");
					}
				}

				System.out.println("done \n \n \n \n");
			} while (msg.getCmd() != 0);
			in.close();
			out.close();
			read.close();
			socket.close();
			File file=new File(System.getProperty("user.dir") + "/Client/Records/");
			for(File f:file.listFiles()){
				f.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}