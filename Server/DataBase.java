package Server;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import Libs.*;

public class DataBase {
	private HashMap<String, ArrayList<Record>> patientHash, divisionHash;
	private HashMap<String, Person> persons;

	public DataBase() {
		patientHash = new HashMap<String, ArrayList<Record>>();
		divisionHash = new HashMap<String, ArrayList<Record>>();
		persons = new HashMap<String, Person>();
		load();
		loadPersonList();
	}

	public ArrayList<Record> getPatientsAllRecords(String patientname) {
		return patientHash.get(patientname);
	}

	public ArrayList<Record> getDivisionRecord(String division) {
		return divisionHash.get(division);
	}

	public Record getSpecificDivisionRecord(String clientname, String division,
			int recordid) {
		ArrayList<Record> list = divisionHash.get(division);
		for (Record r : list) {
			if (r.getId() == recordid
					&& (clientname.equals(r.getDoctor()) || clientname.equals(r
							.getNurse()))) {
				return r;
			}
		}
		return null;
	}

	public void addRecord(String patientname, Record record) {
		String division = record.getDivision();
		ArrayList<Record> patientList = patientHash.get(patientname);
		ArrayList<Record> divisionList = divisionHash.get(division);
		if (patientList == null && divisionList == null) {
			divisionList = new ArrayList<Record>();
			patientList = new ArrayList<Record>();
		} else if (patientList == null && divisionList != null) {
			patientList = new ArrayList<Record>();
		} else if (patientList != null && divisionList == null) {
			divisionList = new ArrayList<Record>();
		}

		divisionList.add(record);
		patientList.add(record);
		patientHash.put(patientname, patientList);
		divisionHash.put(division, divisionList);
		save(patientHash,divisionHash,persons);
	}

	public Person getPerson(String name) {
		return persons.get(name);
	}

	public ArrayList<Record> getAllRecords() {
		Collection<ArrayList<Record>> values = patientHash.values();
		ArrayList<Record> records = new ArrayList<>();
		for (ArrayList<Record> list : values) {
			records.addAll(list);
		}
		return records;
	}

	public void removeJournal(int recordId, String patientname) {
		ArrayList<Record> patientList = patientHash.get(patientname);
		for (int i = 0; i < patientList.size(); i++) {
			Record r = patientList.get(i);
			if (r.getId() == recordId) {
				patientList.remove(i);
			}
		}
		save(patientHash,divisionHash,persons);
	}

	public void createJournal(Record record) {
		String division = record.getDivision();
		String patient = record.getPatient();

		ArrayList<Record> patientList = patientHash.get(patient);
		ArrayList<Record> divisionList = divisionHash.get(division);
		if (patientList == null && divisionList == null) {
			divisionList = new ArrayList<Record>();
			patientList = new ArrayList<Record>();
		} else if (patientList == null && divisionList != null) {
			patientList = new ArrayList<Record>();
		} else if (patientList != null && divisionList == null) {
			divisionList = new ArrayList<Record>();
		}
		patientList.add(record);
		divisionList.add(record);
		patientHash.put(patient, patientList);
		divisionHash.put(division, divisionList);
		
		save(patientHash,divisionHash,persons);

	}

	public void changeJournal(Record r, int recordid) {
		Record record = r;
		String division = record.getDivision();
		String patientname = record.getPatient();
		ArrayList<Record> patientList = patientHash.get(r.getPatient());
		ArrayList<Record> divisionList = divisionHash.get(r.getDivision());
		patientList.add(record);
		divisionList.add(record);
		patientHash.put(patientname,patientList);
		divisionHash.put(division, divisionList);
		save(patientHash,divisionHash,persons);
	}

	public Record getRecord(int recordId) {
		ArrayList<Record> list = getAllRecords();
		for (Record r : list) {
			if (r.getId() == recordId) {
				return r;
			}
		}
		return null;
	}
	
	public ArrayList<Record> getPersonRecord(String name) {
		// TODO Auto-generated method stub
		return patientHash.get(name);
	}

	//kolla inte på koden
	private void save(HashMap<String, ArrayList<Record>> patientHash,
			HashMap<String, ArrayList<Record>> divisionHash,
			HashMap<String, Person> persons) {
		String filePath = System.getProperty("user.dir") + "/Server/Records/Patient";
		File file = new File(filePath);
		if (!file.isFile()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		ObjectOutputStream writer;

		try {
			writer = new ObjectOutputStream(new FileOutputStream(file));
			writer.writeObject(patientHash);
			writer.flush();
			writer.close();
		} catch (IOException e) {

		}
		
		filePath = System.getProperty("user.dir") + "/Server/Records/Division";
		file = new File(filePath);
		if (!file.isFile()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			writer = new ObjectOutputStream(new FileOutputStream(file));
			writer.writeObject(divisionHash);
			writer.flush();
			writer.close();
		} catch (IOException e) {

		}
		
	}
	
	private void load(){
		String filePath = System.getProperty("user.dir") + "/Server/Records/Division";
		File file = new File(filePath);
		if (!file.isFile()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			ObjectInputStream reader = new ObjectInputStream(new FileInputStream(file));
			try {
				divisionHash = (HashMap<String, ArrayList<Record>>) reader.readObject();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();	
		}
		filePath = System.getProperty("user.dir") + "/Server/Records/Patient";
		file = new File(filePath);
		if (!file.isFile()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			ObjectInputStream reader = new ObjectInputStream(new FileInputStream(file));
			try {
				patientHash = (HashMap<String, ArrayList<Record>>) reader.readObject();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	
	private void loadPersonList(){
		persons.put("Doctor One", new Doctor("d1","a"));
		persons.put("Doctor Two", new Doctor("d2","b"));
		persons.put("Nurse One", new Nurse("n1","a"));
		persons.put("Nurse Two", new Nurse("n2","b"));
		persons.put("Big Brother", new GA("GA"));
		persons.put("Patient One",new Patient("Patient One",new ArrayList<Record>()));
		persons.put("Patient Two",new Patient("Patient Two",new ArrayList<Record>()));
		persons.put("Patient Three",new Patient("Patient Three",new ArrayList<Record>()));
	}


}
