package Server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AuditLog {
	private StringBuilder audit;
	private String filePath;
	private File file;
	private SimpleDateFormat sdf;
	private Calendar cal;

	public AuditLog() {
		audit = new StringBuilder();
		sdf = new SimpleDateFormat("HH:mm:ss");
		cal = Calendar.getInstance();
		filePath = System.getProperty("user.dir") + "/Server/AuditLog";
		file = new File(filePath);
		if (!file.isFile()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String input = reader.readLine();
			while (input != null) {
				audit.append(input);
				audit.append("\n");
				input = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void addGetJournalLog(String name,String journal,Boolean opt) {
		audit.append(addAudit(name,journal,opt, 1));
		save(audit);
	}

	public void addWriteJournalLog(String name,String journal,Boolean opt) {
		audit.append(addAudit(name,journal,opt, 2));
		save(audit);
	}

	public void addCreateJournalLog(String name,String journal, Boolean opt) {
		audit.append(addAudit(name,journal,opt, 3));
		save(audit);
	}

	public void addRemoveJournalLog(String name,String journal, Boolean opt) {
		audit.append(addAudit(name,journal,opt, 4));
		save(audit);
	}

	private String addAudit(String name,String journal, Boolean opt1, int opt) {
		StringBuilder returnString = new StringBuilder();
		returnString.append(sdf.format(cal.getTime()) + " :");
		switch (opt) {
		case 1:
			if(opt1){
				returnString.append(name
						+ " has successfully gotten their journals\n");
			}else {
				returnString.append(name
						+ " tried to access their journals but was denied\n");
			}
			break;
		case 2:
			if(opt1){
				returnString.append(name
						+ " has successfully saved "+journal+".\n");
			}else {
				returnString.append(name
						+ " tried to saved "+journal+" but was denied\n");
			}
			break;
		case 3:
			if(opt1){
				returnString.append(name
						+ " has successfully created "+journal+".\n");
			}else {
				returnString.append(name
						+ " tried to create a journal but was denied\n");
			}
			break;
		case 4:
			if(opt1){
			returnString.append(name
					+ " has successfully removed "+journal+". \n");
			} else {
				returnString.append(name
						+ " tried to remove "+journal+" but was denied\n");
			}
			break;
		}
		return returnString.toString();
	}

	private void save(StringBuilder audit) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			writer.write(audit.toString());
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
