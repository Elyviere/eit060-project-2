package Server;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.security.KeyStore;

import javax.net.ServerSocketFactory;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManagerFactory;
import javax.security.cert.X509Certificate;

import Libs.ClientMessage;
import Libs.Record;
import Libs.ServerMessage;

public class Server implements Runnable {
	private ServerSocket serverSocket = null;
	private static int numConnectedClients = 0;
	private static final int EXIT = 0, GET_JOURNAL = 1, WRITE_JOURNAL = 2,
			NEW_JOURNAL = 3, REMOVE_JOURNAL = 4;
	private Commands cmd;

	public Server(ServerSocket ss) throws IOException {
		serverSocket = ss;
		newListener();
		cmd = new Commands();
	}

	@Override
	public void run() {
		try {
			SSLSocket socket = (SSLSocket) serverSocket.accept();
			newListener();
			SSLSession session = socket.getSession();
			X509Certificate cert = session.getPeerCertificateChain()[0];
			String name = cert.getSubjectDN().getName();
			StringBuilder sb = new StringBuilder();
			for (int i = 3; i < name.length(); i++) {
				char ch = name.charAt(i);
				if (ch == ',') {
					break;
				}
				sb.append(ch);
			}
			name = sb.toString();
			System.out.println(name);
			numConnectedClients++;
			System.out.println("client connected");
			System.out.println(numConnectedClients
					+ " concurrent connection(s)\n");

			ObjectOutputStream out = null;
			ObjectInputStream in = null;
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());

			ClientMessage msg;
			do {
				try {
					msg = (ClientMessage) in.readObject();
				} catch (Exception e) {
					//System.err.println("Incorrect command");
					return;
				}

				switch (msg.getCmd()) {
				case GET_JOURNAL:
					out.writeObject(cmd.getJournal(name));
					break;
				case WRITE_JOURNAL:
					out.writeObject(cmd.saveJournal(name, msg.getRecord(), msg.getRecordId()));
					break;
				case NEW_JOURNAL:
					out.writeObject(cmd.createJournal(name, msg));
					break;
				case REMOVE_JOURNAL:
					out.writeObject(cmd.removeJournal(name, msg.getRecordId(),
							msg.getPatientName()));
					break;
				}
			} while (msg.getCmd() != 0);

			in.close();
			out.close();
			socket.close();
			numConnectedClients--;
			System.out.println("client disconnected");
			System.out.println(numConnectedClients
					+ " concurrent connection(s)\n");
		} catch (IOException e) {
			System.out.println("Client died: " + e.getMessage());
			e.printStackTrace();
			return;
		}
	}

	private void newListener() {
		(new Thread(this)).start();
	} // calls run()

	public static void main(String args[]) {
		System.out.println("\nServer Started\n");
		int port = -1;
		if (args.length >= 1) {
			port = Integer.parseInt(args[0]);
		}
		String type = "TLS";
		try {
			ServerSocketFactory ssf = getServerSocketFactory(type);
			ServerSocket ss = ssf.createServerSocket(port);
			((SSLServerSocket) ss).setNeedClientAuth(true); // enables client
															// authentication
			new Server(ss);
		} catch (IOException e) {
			System.out.println("Unable to start Server: " + e.getMessage());
			e.printStackTrace();
		}
	}

	private static ServerSocketFactory getServerSocketFactory(String type) {
		if (type.equals("TLS")) {
			SSLServerSocketFactory ssf = null;
			try { // set up key manager to perform server authentication
				SSLContext ctx = SSLContext.getInstance("TLS");
				KeyManagerFactory kmf = KeyManagerFactory
						.getInstance("SunX509");
				TrustManagerFactory tmf = TrustManagerFactory
						.getInstance("SunX509");
				KeyStore ks = KeyStore.getInstance("JKS");
				KeyStore ts = KeyStore.getInstance("JKS");
				char[] password = "password".toCharArray();

				// måste länka riktigt pathway
				ks.load(new FileInputStream(System.getProperty("user.dir")
						+ "/Server/Stores/serverkeystore"), password); // keystore
																		// password
																		// (storepass)
				ts.load(new FileInputStream(System.getProperty("user.dir")
						+ "/Server/Stores/servertruststore"), password); // truststore
																			// password
																			// (storepass)
				kmf.init(ks, password); // certificate password (keypass)
				tmf.init(ts); // possible to use keystore as truststore here
				ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
				ssf = ctx.getServerSocketFactory();
				return ssf;
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			return ServerSocketFactory.getDefault();
		}
		return null;
	}

}