package Server;

import java.util.ArrayList;

import Libs.*;

public class Commands {
	private DataBase db;
	private int idCounter;
	private AuditLog auditLog;

	public Commands() {
		this.db = new DataBase();
		idCounter = getHighestId();
		auditLog=new AuditLog();
	}

	public ServerMessage getJournal(String name) {
		Person client = db.getPerson(name);
		System.out.println(client.toString());
		ArrayList<Record> records;
		ServerMessage msg;
		if (client instanceof GA) {
			records = db.getAllRecords();
			if (records != null) {
				msg = new ServerMessage("Här har du alla journaler", records);
				auditLog.addGetJournalLog(client.getName(), "", true);
			} else {
				msg = new ServerMessage("Du har ingen journal");
				auditLog.addGetJournalLog(client.getName(), "", false);
			}
		} else if (client instanceof Doctor || client instanceof Nurse) {
			records=db.getDivisionRecord(((HospitalPerson) client).getDivision());
			if(records!=null){
				msg=new ServerMessage("Här har du din divisions journaler",records);
				auditLog.addGetJournalLog(client.getName(), "", true);
			} else {
				msg = new ServerMessage("Du har ingen journal");
				auditLog.addGetJournalLog(client.getName(), "", false);
			}
		} else {
			records = db.getPersonRecord(client.getName());
			if (records != null) {
				msg = new ServerMessage("Här är ditt rekord", records);
				auditLog.addGetJournalLog(client.getName(), "", true);
			} else {
				msg = new ServerMessage("Du har ingen journal");
				auditLog.addGetJournalLog(client.getName(), "", false);
			}
		}
		return msg;
	}

	public ServerMessage createJournal(String doctorname, ClientMessage msg) {
		Person client = db.getPerson(doctorname);
		System.out.println(client.toString());
		if (client instanceof Doctor || client instanceof GA) {
			Record r = new Record(msg.getPatientName(), client.getName(),
					msg.getNurseName(), ((Doctor) client).getDivision(),
					msg.medData(), idCounter);
			db.addRecord(msg.getPatientName(), r);
			idCounter++;
			auditLog.addCreateJournalLog(client.getName(), "journal " + Integer.toString(r.getId()), true);
			return new ServerMessage("Journalen har skapats med ID: " + r.getId() + ". Journalen sparades i " + msg.getFilePath() + ".");
		}
		auditLog.addCreateJournalLog(client.getName(), "", false);
		return new ServerMessage(
				"Du är ej berättigad. Journalen skapades aldrig");	
	}

	public ServerMessage removeJournal(String name, int recordId,
			String patientname) {
		Person client = db.getPerson(name);
		if (client instanceof GA) {
			db.removeJournal(recordId, patientname);
			auditLog.addRemoveJournalLog(client.toString(), "journal "+Integer.toString(recordId), true);
			return new ServerMessage("Journalen har tagits bort");
		}
		auditLog.addRemoveJournalLog(client.toString(),"",false);
		return new ServerMessage(
				"Du är ej berättigad. Journalen har inte blivit borttaget");
	}

	public ServerMessage saveJournal(String name, Record r, int i) {
		Person client = db.getPerson(name);
		if (client instanceof Doctor || client instanceof Nurse) {
			db.changeJournal( r, i);
			auditLog.addWriteJournalLog(client.toString(),"journal "+i,true);
			return new ServerMessage("Journalen har sparats");
		}
		auditLog.addWriteJournalLog(client.toString(),"journal "+i,false);
		return new ServerMessage("Du har inte tillåtelse att spara journaler");
	}
	
	private int getHighestId(){
		ArrayList<Record> records=db.getAllRecords();
		int highestId=0;
		for(Record r:records){
			if(highestId<r.getId()){
				highestId=r.getId();
			}
		}
		return highestId+1;
	}
}
