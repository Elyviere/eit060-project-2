package Libs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

public class ClientMessage implements Serializable {
	
	private static final int EXIT = 0, GET_JOURNAL = 1, WRITE_JOURNAL = 2,
			NEW_JOURNAL = 3, REMOVE_JOURNAL = 4;
	private int cmd, id, recordId;
	private String clientname;
	private Record record;
	private String patientname;
	private String nursename;
	private String input;
	private String medData;
	private ArrayList<Record> records;
	private String filePath;
	
	public ClientMessage(String input, ArrayList<Record> records) {
		this.input=input;
		this.records=records;
		input.trim();
		String[] s = input.split(";");
		try {
			cmd = Integer.parseInt(s[0].trim());
		} catch (NumberFormatException e) {
			System.err.println("Command was not a number, interpreting as cmd = 0 (exit).");
			cmd = EXIT;
		}
		switch (cmd) {
		case EXIT:
			break;
		case GET_JOURNAL:
			break;
		case WRITE_JOURNAL:
			patientname=s[1].trim();
			recordId=Integer.parseInt(s[2].trim());
			for(Record r:records){
				if(r.getId()==recordId){
					record=r;
					record.setData(loadWithWrite(s[2].trim(),patientname));
				}
			}
			break;
		case NEW_JOURNAL:
			patientname = s[1].trim();
			nursename = s[2].trim();
			medData=load(patientname);
			break;
		case REMOVE_JOURNAL:
			patientname = s[1].trim();
			recordId = Integer.parseInt(s[2].trim());
			break;
		}
	}

	public int getCmd() {
		return cmd;
	}
	
	public String getClientName(){
		return clientname;
	}
	
	public Record getRecord(){
		return record;
	}
	

	@Override
	public String toString() {
		String result = "Command: " + cmd;
		if (clientname != null) {
			result += "\tNamn: " + clientname;
		}
		if (id != 0) {
			result += "\tid: " + id;
		}
		return result;
	}

	public int getRecordId() {
		return recordId;
	}

	public String getNurseName() {
		return nursename;
	}

	public String getPatientName() {
		return patientname;
	}

	public String medData() {
		return medData;
	}
	
	private String load(String s){
		String filePath = System.getProperty("user.dir") + "/Client/Records/"+s;
		File file = new File(filePath);
		StringBuilder inputData=new StringBuilder();
		if (!file.isFile()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String input = reader.readLine();
			while (input != null) {
				inputData.append(input);
				inputData.append("\n");
				input = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return inputData.toString();
	}
	
	private String loadWithWrite(String s1,String s2){
		filePath = System.getProperty("user.dir") + "/Client/Records/"+s2+"-"+s1;
		File file = new File(filePath);
		StringBuilder inputData=new StringBuilder();
		if (!file.isFile()) {
			System.err.println("Journal does not exist");
			return null;
		}
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String input = reader.readLine();
			while (input != null) {
				inputData.append(input);
				inputData.append("\n");
				input = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return inputData.toString();
	}

	public String getFilePath() {
		
		return filePath;
	}
}