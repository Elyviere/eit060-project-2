package Libs;

import java.io.Serializable;

public class Record implements Serializable{
	private String doctor;
	private String nurse;
	private String division;
	private String medicalData;
	private String patient;
	private int recordId;
	
	
	public Record(String patient, String doctor, String nurse, String division, String medicalData, int recordId){
		this.doctor = doctor;
		this.nurse = nurse;
		this.division = division;
		this.patient = patient;
		this.medicalData = medicalData;
		this.recordId = recordId;
	}
	
	public String getDivision(){
		return division;
	}
	
	public String getNurse(){
		return nurse;
	}
	
	public String getDoctor(){
		return doctor;
	}
	
	public int getId(){
		return recordId;
	}
	
	public String getPatient(){
		return patient;
	}
	
	public String getData(){
		return medicalData;
	}
	
	public void setData(String data){
		medicalData = data;
	}
	@Override
	public String toString(){
		return "Patient: " + patient + ", Doctor: " + 	
	doctor + ", Nurse: " + nurse + ", Division: " + division + ", Medical Data: " + medicalData;
		
	}
}
