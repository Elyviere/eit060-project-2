package Libs;

import java.io.Serializable;
import java.util.ArrayList;

public class ServerMessage implements Serializable {
	private String message;
	private ArrayList<Record> list;
	
	public ServerMessage(String message, ArrayList<Record> list){
		this.message = message;
		this.list = list;
	}
	
	public ServerMessage(String message){
		this.message = message;
		this.list = null;
	}
	
	public ArrayList<Record> getRecords(){
		return list;
	}

	public String getMessage() {
		// TODO Auto-generated method stub
		return message;
	}
}
