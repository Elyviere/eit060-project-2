package Libs;

public abstract class Person {
	private String name;
	
	public Person(String name){
		this.name = name;
	}
	
	public abstract String getType();

	@Override
	public String toString() {
		return "Name: " + name + ", Type: " + getType();
	}
	
	public String getName(){
		return name;
	}
}
