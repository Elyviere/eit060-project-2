package Libs;

import java.util.ArrayList;

public class Patient extends Person {
	private ArrayList<Record> records;
	private String type;

	public Patient(String name, ArrayList<Record> records) {
		super(name);
		this.records = records;
		this.type = "Patient";
	}
	
	public ArrayList<Record> getMR(){
		return records;
	}
	
	public void addRecord(Record record){
		records.add(record);
	}

	@Override
	public String getType() {
		return type;
	}

}
