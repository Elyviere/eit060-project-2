package Libs;

import java.util.ArrayList;

public abstract class HospitalPerson extends Person {
	protected String division;
	private ArrayList<Patient> patients;
	
	public HospitalPerson(String name,String division) {
		super(name);
		this.division = division;
		patients = new ArrayList<Patient>();
		// TODO Auto-generated constructor stub
	}

	public String getDivision() {
		return division;
	}
	
	public void addPatient(Patient patient){
		patients.add(patient);
	}
	
	public boolean isTreatingPatient(Patient patient){
		for(Patient p:patients){
			if(p.equals(patient)){
				return true;
			}
		}
		return false;
	}
	
	
	
	
}
