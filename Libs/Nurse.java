package Libs;

public class Nurse extends HospitalPerson {
	private String type;
	
	public Nurse(String name, String division) {
		super(name, division);
		this.type = "Nurse";
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}

}
